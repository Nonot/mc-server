FROM bitnami/java
RUN mkdir -p /data/world
WORKDIR /data
VOLUME [ "/data/world" ]
RUN wget https://piston-data.mojang.com/v1/objects/45810d238246d90e811d896f87b14695b7fb6839/server.jar
RUN echo 'eula=true' > eula.txt
COPY "entrypoint.sh" "/entrypoint.sh"
RUN chmod +x "/entrypoint.sh"
COPY server.properties .

ENTRYPOINT [ "/entrypoint.sh" ]
CMD [ "/opt/bitnami/java/bin/java", "-Xmx1024M", "-Xms1024M", "-jar", "server.jar", "nogui" ]
