#!/bin/bash
echo "Copying configuration"
cat /user/custom.properties >> /data/server.properties

if [ -d "/user" ]; then
  cp /user/*.txt /data/
fi
if [ ! -f "/data/whitelist.json" ]; then
 cp /user/whitelist.json /data/
fi
if [ ! -f "/data/ops.json" ]; then
 cp /user/ops.json /data/
fi

echo "Start server"
exec $@
